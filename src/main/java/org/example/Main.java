package org.example;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.parallel.ParallelFlowable;
import io.reactivex.schedulers.Schedulers;

public class Main {

  public static void main(String[] args) {
    System.out.println("Hello world!");


    System.out.println("Hello world! !!!!!!");
  }

  private void asynchronous() throws InterruptedException {
    Observable<Object> observableSync = Observable.create(emitter -> {

      // Publish 100 numbers
      for (int i = 0; i < 100; i++) {
        System.out.println(Thread.currentThread().getName() + " | Publishing = " + i);
        // Publish or emit a value with 10 ms delay
        Thread.sleep(10);
        emitter.onNext(i);
      }
      // When all values or emitted, call complete.
      emitter.onComplete();
    }).subscribeOn(Schedulers.newThread()).observeOn(Schedulers.newThread());
    /*
     * Notice above - subscribeOn & observeOn puts subscriber & publisher/observable
     * on different threads.
     */

    observableSync.subscribe(i -> {
      // Process received value.
      System.out.println(Thread.currentThread().getName() + " | Received = " + i);
      // 100 mills delay to simulate slow subscriber
      Thread.sleep(100);
    });

    // Since publisher & subscriber run on different thread than main thread, keep
    // main thread active for 5 seconds.
    Thread.sleep(5000);
  }

  private void parallel() {
    ParallelFlowable<Object> flowableParallel = Flowable.create(emitter -> {

      // Publish 100 numbers
      for (int i = 0; i < 100; i++) {
        emitter.onNext(i);
        // Publish or emit a value with 10 ms delay
        Thread.sleep(10);
      }
      // When all values or emitted, call complete.
      emitter.onComplete();
    }, BackpressureStrategy.BUFFER).parallel().runOn(Schedulers.computation()).map(i -> {
      int number = (int) i;
      System.out.println(Thread.currentThread().getName() + " | Sending square of " + number);

      // Processing - Square all numbers & provide to subscribers.
      return number * number;
    });
    /*
     * Notice above - parallel() & runOn() combination puts further processing in
     * parallel threads.
     */

    // Now here we will merge all parallel threads back in round-robin order so all
    // squares are printed.
    flowableParallel.sequential().blockingSubscribe(
        i -> System.out.println(Thread.currentThread().getName() + " | Received square = " + i));

  }

  private void synchronous() {
    Observable<Object> observableSync = Observable.create(emitter -> {

      // Publish 100 numbers
      for (int i = 0; i < 10; i++) {
        System.out.println(Thread.currentThread().getName() + " | Publishing = " + i);
        // Publish or emit a value.
        emitter.onNext(i);
      }
      // When all values or emitted, call complete.
      emitter.onComplete();
    });

    observableSync.subscribe(i -> {
      // Process received value.
      System.out.println(Thread.currentThread().getName() + " | Received = " + i);
      // 100 mills delay to simulate slow subscriber
      Thread.sleep(100);
    });
  }
}